const path = require("path");

module.exports = {
  mode: "development",
  entry: path.resolve(__dirname, "src/index.jsx"),
  output: {
    path: path.resolve(__dirname, "dist"),
    filename: "main.js",
  },
  module: {
    rules: [
      {
        test: /\.jsx?/,
        use: {
          loader: "babel-loader",
          options: {
            presets: ["@babel/preset-env", "@babel/preset-react"],
            plugins: [
              ["@babel/plugin-proposal-decorators", { legacy: true }],
              "@babel/plugin-proposal-class-properties",
            ]
          },
        },
      },
    ],
  },
  // devtool: "inline-source-map",
  resolve: {
    alias: {
      mobx: path.resolve(__dirname, 'node_modules/mobx/lib/mobx.es6.js')
    }
  }
};
