import React, { Component } from 'react'
import { observable, action, computed, trace } from 'mobx'
import ReactDOM from 'react-dom'
import { observer } from 'mobx-react'
import 'mobx-react-lite/batchingForReactDom'

class Todo {
  id = Math.random()
  @observable title = ''
  @observable done = false
  constructor(title) { this.title = title }
  @action.bound toggle() { this.done = !this.done }
}

class Store {
  @observable todos = []
  @observable input = ''
  @action.bound inputChange(value) { this.input = value || '' }
  @action.bound createTodo() { this.todos.push(new Todo(this.input)) }
  @computed get remains() { return this.todos.filter(todo => !todo.done).length }
  @action.bound removeTodo(todo) { this.todos.remove(todo) }
}

const store = new Store()

/**
 * 函数声明会前置，而函数表达式只能往前写
 * 函数不能应用装饰器，所以只能用表达式
 * 所以总结起来要使用无状态组件，在使用前用表达式定义函数
 */
const Edit = observer(({ store }) => {
  trace()
  return (
    <input type="text" value={store.input} onChange={
      e => { store.inputChange(e.target.value) }
    } />
  )
})

const Footer = observer(({ store }) => {
  trace()
  return <div>TODO remains: {store.remains}</div>
})

const List = observer(({ store }) => {
  trace()
  return (
    store.todos.map(item => (
      <TodoItem key={item.id} todo={item}>
        {} <span onClick={() => store.removeTodo(item)}>Delete</span>
      </TodoItem>
    ))
  )
})

const TodoItem = observer(({ todo, children }) => {
  trace()
  return (
    <li>
      <input type="checkbox" checked={todo.done} onChange={todo.toggle} />
      {} {todo.title}
      {children}
    </li>
  )
})

@observer
class TodoList extends Component {
  createTodo = e => {
    e.preventDefault()
    this.props.store.createTodo()
    this.props.store.inputChange()
  }
  render() {
    trace()
    return (
      <>
        <div>
          <form onSubmit={this.createTodo}><Edit store={this.props.store} /></form>
        </div>
        <ul><List store={this.props.store} /></ul>
        <Footer store={this.props.store} />
      </>
    )
  }
}

ReactDOM.render(<TodoList store={store} />, document.getElementById('root'))

/**
 * 实现最小化的 DOM 更新
 * mobx-react、props 更改、调用 setState 这三种方法确定 render() 是否重新执行
 * 而 render() 后的虚拟 DOM 来决定哪些地方需要更新
 * 因此最小化更新需要保证以下几点：
 * render 本身不能太费时，否则元素生成慢
 * render 生成的 React 元素不能太大太复杂，否则 diff 算法慢
 * 尽可能减少 render 的调用，特别是不需要更新 DOM 的 render
 */

/**
 * 性能优化最佳实践（将变化尽量限制在局部）
 *
 * 经常重新渲染的组件单独成为一个组件
 *   比如会被更改的列表，其重新 render() 的可能性非常大
 *   将大组件中动态的部分提出，静态部分单独作为一个组件
 * 组件尽量只进行同一层级的依赖
 *   如上面如果同时依赖 todo[] 和 todo，那么任何一个发生更改都会导致重新渲染
 * 降低渲染的频次
 *   使用防抖和节流技术
 * 父组件重新渲染的代价转移到子组件
 *   父组件重新渲染一定会牵动子组件渲染（子组件没有做 shouldComponentUpdate 优化的时候）
 *   而子组件重新渲染一定不会牵动父组件
 *   如果将组件看成一棵树的话，那就尽可能将重新渲染的组件分布到叶子节点
 */

/**
 * 列表如何 render 优化
 *
 * 如果 key 在现有的对象中，那么跳过该对象的重新生成
 * 或者生成之后，跳过比较过程
 * 或者完全生成之后，获得最小差异的 diff 方式（React 的做法）
 * 第一种做法是最佳的，但必须保证相同 key 的结果永远都是不变的
 */

/*
差异最小的 diff 需要 key 进行协助（提示）
否则最小 diff 算法时间消耗不能保证

1 2 3 4 5
A B C D E

最小的差异 diff，只删除 1 次 3 号

1 2 4 5
A B D E

比较大的差异 diff 方式，删除 1 次 5 号，更新 2 次（如使用下标）

1 2 3 4
A B D E
*/
