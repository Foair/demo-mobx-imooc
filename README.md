MobX 入门基础教程 - 慕课网
https://www.imooc.com/learn/1012

## MobX 和 Redux 的比较

Redux 中概念众多，自身提供的功能少，需要引入各种中间件来增强功能。而 MobX 自身的集成度已经较高，内置了丰富的 API，减少了开发的依赖。Redux 中的每个部分都对应着一些代码，因此修改一个地方，往往意味着其他地方也需要修改，代码量比较多。

## 基本概念

当 MobX 接管了一个组件，并且当一些状态发生了变更之后，它能明确地知道哪些组件应该重新渲染。而不需要手动干预 `shouldCompoenntUpdate`。因为修改 state 是牵一发动全身的。

状态变化引起的副作用应该被自动触发。这里的副作用是所有依赖该状态的任何代码，包括但不限于组件和网络请求。并且依赖收集的过程是自动发生的，并不需要任何显式地指出。

## class 语法

🌺 任务 `d6b3a5eab8a7796c0c7e095a6fabf9c3234acb66`

>  配置 webpack 创建 `/index.html` 并引入编译后的 `main.js` 并在浏览器中展示结果。

注意装饰器的插件要在类属性插件之前。

🌺 任务 `34f643727a48f6fbab250482c35d7282980616ce`

> 通过两个函数实现继承并实现方法的覆写。

## 装饰器语法

`mobx-react` 本质是将组件的 `render` 包装成 `autorun`。